SELECT
	(LOWER("Mail"."Box"."User") || '@' || LOWER("Dns"."Domain"."Name")) AS "user",
	"Mail"."Box"."Password" AS "password",
	5000 AS "userdb_uid",
	5000 AS "userdb_gid",
	('/home/vmail/' || "public"."Account"."Username" || '/' || LOWER("Dns"."Domain"."Name") || '/' || LOWER("Mail"."Box"."User")) AS "userdb_home"

FROM
	"Mail"."Box"

INNER JOIN
	"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Mail"."Box"."DomainID"
	)

INNER JOIN 
	"public"."Account" ON (
		"public"."Account"."ID" = "Box"."AccountID"
	)

WHERE
	LOWER("Mail"."Box"."User") || '@' || LOWER("Dns"."Domain"."Name")) = LOWER('%u')
		AND "Mail"."Box"."Active" = true
;


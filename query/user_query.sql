SELECT
	5000 AS "uid",
	5000 AS "gid",
	('/home/vmail/' || "public"."Account"."Username" || '/' || LOWER("Dns"."Domain"."Name") || '/' || LOWER("Mail"."Box"."User")) AS "home",
	('maildir:/home/vmail/' || "public"."Account"."Username" || '/' || LOWER("Dns"."Domain"."Name") || '/' || LOWER("Mail"."Box"."User")) AS "mail",
	('*:bytes=' || "Mail"."Box"."Quota") AS "quota_rule"

FROM
	"Mail"."Box"

INNER JOIN
	"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Mail"."Box"."DomainID"
	)

INNER JOIN
	"public"."Account" ON (
		"public"."Account"."ID" = "Mail"."Box"."AccountID"
	)

WHERE
	LOWER("Mail"."Box"."User") || '@' || LOWER("Dns"."Domain"."Name")) = LOWER('%u')
		AND "Mail"."Box"."Active" = true
;


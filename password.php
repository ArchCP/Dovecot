<?php
// Set the password
$strPassword = trim($argv[1]);
// Set the salt
$strSalt     = substr(sha1(rand()), 0, 16);
// Set the hashed password
$strHash     = sprintf('{SHA512-CRYPT}%s', crypt($strPassword, sprintf('$6$%s', $strSalt)));
// Send the password to the user
printf('%s%s%s%s', PHP_EOL, $strHash, PHP_EOL, PHP_EOL);
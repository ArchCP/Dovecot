SELECT
	(LOWER("Mail"."Box"."User") || '@' || LOWER("Dns"."Domain"."Name")) AS "user"

FROM
	"Mail"."Box"

INNER JOIN
	"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Mail"."Box"."DomainID"
	)

WHERE
	"Mail"."Box"."Active" = true
;

